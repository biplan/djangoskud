# coding: utf-8
from django.db import models


class Client(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    second_name = models.CharField(max_length=50)
    password = models.CharField(max_length=20)
    def getFIO(self):
        return self.first_name + " " + self.second_name + " " + self.last_name
    def __unicode__(self):
        return self.getFIO()

class Place(models.Model):
    caption = models.CharField(max_length=100)
    adres = models.CharField(max_length=256)
    type = models.CharField(max_length=20, default="public")
    def __unicode__(self):
        return self.caption

class Statistic(models.Model):
    action = models.CharField(max_length=20)
    date = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(Client)
    place = models.ForeignKey(Place)
    def __unicode__(self):
        return self.user.getFIO() + " " + self.place.caption + " " + self.action + " " + self.date.strftime("%H:%M:%S %Y") 

class List(models.Model):
    place = models.ForeignKey(Place)
    user = models.ForeignKey(Client)
    status = models.CharField(max_length=20)
        