from django.http import HttpResponse
from django.http import Http404
from django.template import Context, loader
from skud.models import Client, Place, Statistic, List

def index(request):
    return HttpResponse('Hello World!')

#def inplace(request, place_id=None):
#    if place_id != None:
#        userInPlace = Statistic.objects.filter(
#           place__id=place_id
#        ).order_by('-date')[:50]
#        
#        template = loader.get_template('place/index.html')
#        
#        context = Context({
#            'userInPlace' : userInPlace,
#        })
#        
#        #return HttpResponse("Looking place by id: %s" % userInPlace[0])
#        return HttpResponse(template.render(context))
#    else:
#        return HttpResponse("All O'K")

def info(request, entityName, item_id):
    if entityName == 'user':
        template = loader.get_template('place/userinfo.html')
        u = Client.objects.get(pk=item_id)
        st = Statistic.objects.filter(user=u).order_by('-date').exclude(place__type="private")[:100]
        context = Context({
            'user' : u,
            'stata' : st,
        })
    elif entityName == 'place':
        template = loader.get_template('place/placeinfo.html')
        p = Place.objects.get(pk=item_id)
        if p.type == 'public':
            st = Statistic.objects.filter(place=p).order_by('-date')[:100]
        else:
            st = None
            #raise Http404
            
        context = Context({
            'place' : p,
            'stata' : st,
        })
    return HttpResponse(template.render(context))

def access(request, action, place_id, user_id):
    place = Place.objects.get(pk=place_id)
    user = Client.objects.get(pk=user_id)
    accessStatus = "deny"
    if place.type == 'public':
        accessStatus = "grant"        
    if (List.objects.filter(user=user, place=place, status="grant") and place.type != "public"):
        accessStatus = "grant"
    if (List.objects.filter(user=user, place=place, status="deny")):
        accessStatus = "deny"
    if (accessStatus == "grant"):
        st = Statistic(user=user, place=place, action=action)
        st.save()
        return HttpResponse("access granted")
    else:
        return HttpResponse("access DENIEDE")
        
