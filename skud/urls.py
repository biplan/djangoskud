from django.conf.urls import patterns, url

from skud import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    #url(r'^inplace/$', views.inplace, name="inpace"),
    #url(r'^inplace/(?P<place_id>\d+)/$', views.inplace, name="inplace"),
    url(r'^info/(?P<entityName>\w+)/(?P<item_id>\d+)/$', views.info, name="userinfo"),
    url(r'^access/(?P<action>\w+)/(?P<place_id>\d+)/(?P<user_id>\d+)/$', views.access, name="useraccess"),
    
    #url(r'^ui/$', views.userinfo, name="userinfo"),
    #url(r'^(?P<place_id>\d+)/inplace/$', views.inplace, name="inplace"),
    #url(r'^(?P<user_id>\d+)/userinfo/$', views.userinfo, name="userinfo"),
    
)