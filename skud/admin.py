# coding: utf-8
from django.contrib import admin
from skud import models

class ClientAdmin(admin.ModelAdmin):
    pass

class PlaceAdmin(admin.ModelAdmin):
    pass

class StatisticAdmin(admin.ModelAdmin):
    pass



admin.site.register(models.Client, ClientAdmin)
admin.site.register(models.Place, PlaceAdmin)
admin.site.register(models.Statistic, StatisticAdmin)

